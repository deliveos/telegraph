package telegraph

import "gitlab.com/deliveos/telegraph/models"

// ChatConfig is base type for all chat config types.
type ChatConfig struct {
	ChatID                   int64       // Unique identifier for the target chat or username of the target channel (in the format @channelusername)
	ProtectContent           bool        // Protects the contents of the sent message from forwarding and saving
	ReplyToMessageID         int64       // If the message is a reply, ID of the original message
	ReplyMarkup              interface{} // Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	DisableNotification      bool        // Sends the message silently. Users will receive a notification with no sound.
	AllowSendingWithoutReply bool        // Pass True, if the message should be sent even if the specified replied-to message is not found
}

// func (chat *ChatConfig) params() (Params, error) {
// 	params := make(Params)

// 	params.AddNonZero64("chat_id", chat.ChatID)
// 	params.AddNonZero64("reply_to_message_id", chat.ReplyToMessageID)
// 	params.AddBool("disable_notification", chat.DisableNotification)
// 	params.AddBool("allow_sending_without_reply", chat.AllowSendingWithoutReply)
// 	params.AddBool("protect_content", chat.ProtectContent)

// 	err := params.AddInterface("reply_markup", chat.ReplyMarkup)

// 	return params, err
// }

// Contains an information about a SendMessage request.
type SendMessageConfing struct {
	ParseMode             string                 // Mode for parsing entities in the message text. See formatting options (https://core.tlgr.org/bots/api#formatting-options) for more details.
	Entities              []models.MessageEntity // A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
	DisableWebPagePreview bool                   // Disables link previews for links in this message
	ReplyMarkup           interface{}
}

func (smc *SendMessageConfing) params() (Params, error) {
	var params = make(Params)

	params.AddBool("disable_web_page_preview", smc.DisableWebPagePreview)
	params.AddNonEmpty("parse_mode", smc.ParseMode)
	params.AddInterface("reply_markup", smc.ReplyMarkup)
	err := params.AddInterface("entities", smc.Entities)

	return params, err
}

// Contains an information about a getUpdates (https://core.tlgr.org/bots/api#getupdates) request.
type GetUpdatesConfig struct {
	Offset         int64    // Identifier of the first update to be returned. Must be greater by one than the highest among the identifiers of previously received updates. By default, updates starting with the earliest unconfirmed update are returned. An update is considered confirmed as soon as getUpdates is called with an offset higher than its update_id. The negative offset can be specified to retrieve updates starting from -offset update from the end of the updates queue. All previous updates will forgotten.
	Limit          int64    // Limits the number of updates to be retrieved. Values between 1-100 are accepted. Defaults to 100.
	Timeout        int64    // Timeout in seconds for long polling. Defaults to 0, i.e. usual short polling. Should be positive, short polling should be used for testing purposes only.
	AllowedUpdates []string // A JSON-serialized list of the update types you want your bot to receive. For example, specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types. See Update for a complete list of available update types. Specify an empty list to receive all update types except chat_member (default). If not specified, the previous setting will be used.
}

func (gus *GetUpdatesConfig) params() (Params, error) {
	params := make(Params)

	params.AddNonZero("offset", int(gus.Offset))
	params.AddNonZero64("limit", gus.Limit)
	params.AddNonZero64("timeout", gus.Timeout)
	err := params.AddInterface("allowed_updates", gus.AllowedUpdates)

	return params, err
}
