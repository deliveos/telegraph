package linq

type Comparable interface {
	int | string
}

func SelectFirstOrDefault[T interface{}](s []T, f func(T) bool) *T {
	for _, item := range s {
		if f(item) {
			return &item
		}
	}
	return nil
}

func SelectFirstOrDefaultMap[T interface{}, K Comparable](s map[K]T, f func(T) bool) *T {
	for _, item := range s {
		if f(item) {
			return &item
		}
	}
	return nil
}
