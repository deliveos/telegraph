package models

/*
	This object represents a unique message identifier.

Source: https://core.tlgr.org/bots/api#messageid
*/
type MessageID struct {
	MessageID int `json:"message_id"` // Unique message identifier
}
