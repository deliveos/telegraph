package models

/*
	Represents the rights of an administrator in a chat.

Source: https://core.tlgr.org/bots/api#chatadministratorrights
*/
type ChatAdministratorRights struct {
	IsAnonymous         bool  `json:"is_anonymous"`           // True, if the user's presence in the chat is hidden
	CanManageChat       bool  `json:"can_manage_chat"`        // True, if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
	CanDeleteMessages   bool  `json:"can_delete_messages"`    // True, if the administrator can delete messages of other users
	CanManageVideoChats bool  `json:"can_manage_video_chats"` // True, if the administrator can manage video chats
	CanRestrictMembers  bool  `json:"can_restrict_members"`   // True, if the administrator can restrict, ban or unban chat members
	CanPromoteMembers   bool  `json:"can_promote_members"`    // True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by the user)
	CanChangeInfo       bool  `json:"can_change_info"`        // True, if the user is allowed to change the chat title, photo and other settings
	CanInviteUsers      bool  `json:"can_invite_users"`       // True, if the user is allowed to invite new users to the chat
	CanPostMessages     *bool `json:"can_post_messages"`      // Optional. True, if the administrator can post in the channel; channels only
	CanEditMessages     *bool `json:"can_edit_messages"`      // Optional. True, if the administrator can edit messages of other users and can pin messages; channels only
	CanPinMessages      *bool `json:"can_pin_messages"`       // Optional. True, if the user is allowed to pin messages; groups and supergroups only
}
