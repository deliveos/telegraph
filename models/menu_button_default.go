package models

/*
	Describes that no specific value for the menu button was set.

Source: https://core.tlgr.org/bots/api#menubuttondefault
*/
type MenuButtonDefault struct {
	Type string `json:"type"` // Type of the button, must be "default"
}
