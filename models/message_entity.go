package models

/*
	This object represents one special entity in a text message. For example, hashtags, usernames, URLs, etc.

Source: https://core.tlgr.org/bots/api#messageentity
*/
type MessageEntity struct {
	Type     MessageEntityType `json:"type"`     // Type of the entity. Currently, can be "mention" (@username), "hashtag" (#hashtag), "cashtag" ($USD), "bot_command" (/start@jobs_bot), "url" (https://tlgr.org), "email" (do-not-reply@telegram.org), "phone_number" (+1-212-555-0123), "bold" (bold text), "italic" (italic text), "underline" (underlined text), "strikethrough" (strikethrough text), "spoiler" (spoiler message), "code" (monowidth string), "pre" (monowidth block), "text_link" (for clickable text URLs), "text_mention" (for users without usernames)
	Offset   int16             `json:"offset"`   // Offset in UTF-16 code units to the start of the entity
	Length   int16             `json:"length"`   // Length of the entity in UTF-16 code units
	Url      string            `json:"url"`      // Optional. For "text_link" only, URL that will be opened after user taps on the text
	User     User              `json:"user"`     // Optional. For "text_mention" only, the mentioned user
	Language string            `json:"language"` // Optional. For "pre" only, the programming language of the entity text
}

// IsCommand returns true if the type of the message entity is "bot_command".
func (e MessageEntity) IsCommand() bool {
	return e.Type == "bot_command"
}
