package models

// Represents a menu button, which launches a Web App (https://core.tlgr.org/bots/webapps).
//
// Source: https://core.tlgr.org/bots/api#menubuttonwebapp
type MenuButtonWebApp struct {
	Type   string     `json:"type"`    // Type of the button, must be "web_app"
	Text   string     `json:"text"`    //	Text on the button
	WebApp WebAppInfo `json:"web_app"` // Description of the Web App that will be launched when the user presses the button. The Web App will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery (https://core.tlgr.org/bots/api#answerwebappquery).
}
