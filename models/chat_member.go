package models

/*
	This object contains information about one member of a chat. Currently, the following 6 types of chat members are supported:

	   ChatMemberOwner
	   ChatMemberAdministrator
	   ChatMemberMember
	   ChatMemberRestricted
	   ChatMemberLeft
	   ChatMemberBanned

Source: https://core.tlgr.org/bots/api#chatmember
*/
type ChatMember interface{}
