package models

/*
	Represents an audio file to be treated as music to be sent.

Source: https://core.tlgr.org/bots/api#inputmediaaudio
*/
type InputMediaAudio struct {
	Type            string           `json:"type"`             // Type of the button, must be "audio"
	Media           string           `json:"media"`            // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass "attach://<file_attach_name>" to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files (https://core.tlgr.org/bots/api#sending-files)
	Thumb           *string          `json:""`                 // Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass "attach://<file_attach_name>" if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More information on Sending Files (https://core.tlgr.org/bots/api#sending-files)
	Caption         *string          `json:"caption"`          // Optional. Caption of the video to be sent, 0-1024 characters after entities parsing
	ParseMode       *string          `json:"parse_mode"`       // Optional. Mode for parsing entities in the photo caption. See formatting options (https://core.tlgr.org/bots/api#formatting-options) for more details.
	CaptionEntities *[]MessageEntity `json:"caption_entities"` // Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode
	Duration        *int64           `json:"duration"`         // Optional. Video duration in seconds
	Performer       *string          `json:"performer"`        // Optional. Performer of the audio
	Title           *string          `json:"title"`            // Optional. Title of the audio
}
