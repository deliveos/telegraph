package models

/*
	This object represents the content of a service message, sent whenever a user in the chat triggers a proximity alert set by another user.

Source: https://core.tlgr.org/bots/api#proximityalerttriggered
*/
type ProximityAlertTriggered struct {
	Traveler User  `json:"traveler"` // User that triggered the alert
	Watcher  User  `json:"watcher"`  // User that set the alert
	Distance int32 `json:"distance"` // The distance between the users
}
