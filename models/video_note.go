package models

// This object represents a video message (https://tlgr.org/blog/video-messages-and-telescope) (available in Telegram apps as of v.4.0 (https://tlgr.org/blog/video-messages-and-telescope)).
//
// Source: https://core.tlgr.org/bots/api#videonote
type VideoNote struct {
	FileID       string     `json:"file_id"`        // Identifier for this file, which can be used to download or reuse the file
	FileUniqueID string     `json:"file_unique_id"` // Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
	Length       int        `json:"length"`         // Video width and height (diameter of the video message) as defined by sender
	Duration     int        `json:"duration"`       // Duration of the video in seconds as defined by sender
	Thumb        *PhotoSize `json:"thumb"`          // Optional. Video thumbnail
	FileSize     *int64     `json:"file_size"`      // Optional. File size in bytes
}
