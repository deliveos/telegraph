package models

// Represents a chat member (https://core.tlgr.org/bots/api#chatmember) that owns the chat and has all administrator privileges.
//
// Source: https://core.tlgr.org/bots/api#chatmember
type ChatMemberOwner struct {
	Status      string  `json:"status"`       // The member's status in the chat, always "creator"
	User        User    `json:"user"`         // Information about the user
	IsAnonymous bool    `json:"is_anonymous"` // True, if the user's presence in the chat is hidden
	CustomTitle *string `json:"custom_title"` // Optional. Custom title for this user
}
