package models

// This object represents one button of an inline keyboard. You must use exactly one of the optional fields.
//
// Source: https://core.tlgr.org/bots/api#inlinekeyboardbutton
type InlineKeyboardButton struct {
	Text                         string        `json:"text"`                             // Label text on the button
	URL                          *string       `json:"url"`                              // Optional. HTTP or tg:// URL to be opened when the button is pressed. Links tg://user?id=<user_id> can be used to mention a user by their ID without using a username, if this is allowed by their privacy settings.
	CallbackData                 *string       `json:"callback_data"`                    // Optional. Data to be sent in a callback query (https://core.tlgr.org/bots/api#callbackquery) to the bot when button is pressed, 1-64 bytes
	WebApp                       *WebAppInfo   `json:"web_app"`                          // Optional. Description of the Web App that will be launched when the user presses the button. The Web App (https://core.tlgr.org/bots/webapps) will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery (https://core.tlgr.org/bots/api#answerwebappquery). Available only in private chats between a user and the bot.
	LoginURL                     *string       `json:"login_url"`                        // Optional. An HTTPS URL used to automatically authorize the user. Can be used as a replacement for the Telegram Login Widget (https://core.tlgr.org/widgets/login).
	SwitchInlineQuery            *string       `json:"switch_inline_query"`              // Optional. If set, pressing the button will prompt the user to select one of their chats, open that chat and insert the bot's username and the specified inline query in the input field. May be empty, in which case just the bot's username will be inserted. Note: This offers an easy way for users to start using your bot in inline mode (https://core.tlgr.org/bots/inline) when they are currently in a private chat with it. Especially useful when combined with switch_pm… (https://core.tlgr.org/bots/api#answerinlinequery) actions - in this case the user will be automatically returned to the chat they switched from, skipping the chat selection screen.
	SwitchInlineQueryCurrentChat *string       `json:"switch_inline_query_current_chat"` // Optional. If set, pressing the button will insert the bot's username and the specified inline query in the current chat's input field. May be empty, in which case only the bot's username will be inserted.
	CallbackGame                 *CallbackGame `json:"callback_game"`                    // Optional. Description of the game that will be launched when the user presses the button. NOTE: This type of button must always be the first button in the first row.
	Pay                          *bool         `json:"pay"`                              // Optional. Specify True, to send a Pay button (https://core.tlgr.org/bots/api#payments). NOTE: This type of button must always be the first button in the first row and can only be used in invoice messages.
}

type InlineKeyboardButtonOptions struct {
	URL                          string       `json:"url"`                              // Optional. HTTP or tg:// URL to be opened when the button is pressed. Links tg://user?id=<user_id> can be used to mention a user by their ID without using a username, if this is allowed by their privacy settings.
	CallbackData                 string       `json:"callback_data"`                    // Optional. Data to be sent in a callback query (https://core.tlgr.org/bots/api#callbackquery) to the bot when button is pressed, 1-64 bytes
	WebApp                       WebAppInfo   `json:"web_app"`                          // Optional. Description of the Web App that will be launched when the user presses the button. The Web App (https://core.tlgr.org/bots/webapps) will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery (https://core.tlgr.org/bots/api#answerwebappquery). Available only in private chats between a user and the bot.
	LoginURL                     string       `json:"login_url"`                        // Optional. An HTTPS URL used to automatically authorize the user. Can be used as a replacement for the Telegram Login Widget (https://core.tlgr.org/widgets/login).
	SwitchInlineQuery            string       `json:"switch_inline_query"`              // Optional. If set, pressing the button will prompt the user to select one of their chats, open that chat and insert the bot's username and the specified inline query in the input field. May be empty, in which case just the bot's username will be inserted. Note: This offers an easy way for users to start using your bot in inline mode (https://core.tlgr.org/bots/inline) when they are currently in a private chat with it. Especially useful when combined with switch_pm… (https://core.tlgr.org/bots/api#answerinlinequery) actions - in this case the user will be automatically returned to the chat they switched from, skipping the chat selection screen.
	SwitchInlineQueryCurrentChat string       `json:"switch_inline_query_current_chat"` // Optional. If set, pressing the button will insert the bot's username and the specified inline query in the current chat's input field. May be empty, in which case only the bot's username will be inserted.
	CallbackGame                 CallbackGame `json:"callback_game"`                    // Optional. Description of the game that will be launched when the user presses the button. NOTE: This type of button must always be the first button in the first row.
	Pay                          bool         `json:"pay"`                              // Optional. Specify True, to send a Pay button (https://core.tlgr.org/bots/api#payments). NOTE: This type of button must always be the first button in the first row and can only be used in invoice messages.
}

func NewInlineKeyboardButton(text string, options InlineKeyboardButtonOptions) *InlineKeyboardButton {
	return &InlineKeyboardButton{
		Text:                         text,
		URL:                          &options.URL,
		CallbackData:                 &options.CallbackData,
		WebApp:                       &options.WebApp,
		LoginURL:                     &options.LoginURL,
		SwitchInlineQuery:            &options.SwitchInlineQuery,
		SwitchInlineQueryCurrentChat: &options.SwitchInlineQueryCurrentChat,
		CallbackGame:                 &options.CallbackGame,
		Pay:                          &options.Pay,
	}
}
