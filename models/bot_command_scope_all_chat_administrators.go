package models

// Represents the scope of bot commands, covering all group and supergroup chat administrators.
//
// Source: https://core.tlgr.org/bots/api#botcommandscopeallchatadministrators
type BotCommandScopeAllChatAdministrators struct {
	Type string `json:"type"` // Scope type, must be "all_chat_administrators"
}
