package models

/*
	Represents a menu button, which opens the bot's list of commands.

Source: https://core.tlgr.org/bots/api#menubuttoncommands
*/
type MenuButtonCommands struct {
	Type string `json:"type"` // Type of the button, must be "commands"
}
