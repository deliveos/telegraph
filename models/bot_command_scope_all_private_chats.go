package models

// Represents the scope (https://core.tlgr.org/bots/api#botcommandscope) of bot commands, covering all private chats.
//
// Source: https://core.tlgr.org/bots/api#botcommandscopeallprivatechats
type BotCommandScopeAllPrivateChats struct {
	Type string `json:"type"` // Scope type, must be "all_private_chats"
}
