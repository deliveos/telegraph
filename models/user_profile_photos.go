package models

/*
	This object represent a user's profile pictures.

Source: https://core.tlgr.org/bots/api#userprofilephotos
*/
type UserProfilePhotos struct {
	TotalCount int32         `json:"total_count"` // Total number of profile pictures the target user has
	Photos     [][]PhotoSize `json:"photos"`      // Requested profile pictures (in up to 4 sizes each)
}
