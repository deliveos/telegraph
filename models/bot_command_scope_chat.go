package models

// Represents the scope (https://core.tlgr.org/bots/api#botcommandscope) of bot commands, covering a specific chat.
//
// Source: https://core.tlgr.org/bots/api#botcommandscopechat
type BotCommandScopeChat struct {
	Type   string `json:"type"`    // Scope type, must be "chat"
	ChatID int64  `json:"chat_id"` // Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
}
