package models

// Describes a Web App (https://core.tlgr.org/bots/webapps).
//
// Source: https://core.tlgr.org/bots/api#webappinfo
type WebAppInfo struct {
	URL string `json:"url"` // An HTTPS URL of a Web App to be opened with additional data as specified in Initializing Web Apps (https://core.tlgr.org/bots/webapps#initializing-web-apps)
}
