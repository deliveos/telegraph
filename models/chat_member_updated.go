package models

/*
	This object represents changes in the status of a chat member.

Source: https://core.tlgr.org/bots/api#chatmemberupdated
*/
type ChatMemberUpdated struct {
	Chat          Chat            `json:"chat"`            // Chat the user belongs to
	From          User            `json:"from"`            // Performer of the action, which resulted in the change
	Date          int64           `json:"date"`            // Date the change was done in Unix time
	OldChatMember ChatMember      `json:"old_chat_member"` // Previous information about the chat member
	NewChatMember ChatMember      `json:"new_chat_member"` // New information about the chat member
	InviteLink    *ChatInviteLink `json:"invite_link"`     // Optional. Chat invite link, which was used by the user to join the chat; for joining by invite link events only.
}
