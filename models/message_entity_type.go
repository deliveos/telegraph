package models

/*
Type of the entity. Currently, can be:

	"mention" (@username),
	"hashtag" (#hashtag),
	"cashtag" ($USD),
	"bot_command" (/start@jobs_bot),
	"url" (https://tlgr.org),
	"email" (do-not-reply@telegram.org),
	"phone_number" (+1-212-555-0123),
	"bold" (bold text),
	"italic" (italic text),
	"underline" (underlined text),
	"strikethrough" (strikethrough text),
	"spoiler" (spoiler message),
	"code" (monowidth string),
	"pre" (monowidth block),
	"text_link" (for clickable text URLs),
	"text_mention" (for users without usernames)
*/
type MessageEntityType string

const (
	MessageEntityTypeMention       MessageEntityType = "mention"
	MessageEntityTypeHashtag       MessageEntityType = "hashtag"
	MessageEntityTypeCashtag       MessageEntityType = "cashtag"
	MessageEntityTypeBotCommand    MessageEntityType = "bot_command"
	MessageEntityTypeURL           MessageEntityType = "url"
	MessageEntityTypeEmail         MessageEntityType = "email"
	MessageEntityTypePhoneNumber   MessageEntityType = "phone_number"
	MessageEntityTypeBold          MessageEntityType = "bold"
	MessageEntityTypeItalic        MessageEntityType = "italic"
	MessageEntityTypeUnderline     MessageEntityType = "underline"
	MessageEntityTypeStrikethrough MessageEntityType = "strikethrough"
	MessageEntityTypeSpoiler       MessageEntityType = "spoiler"
	MessageEntityTypeCode          MessageEntityType = "code"
	MessageEntityTypePre           MessageEntityType = "pre"
	MessageEntityTypeTextLink      MessageEntityType = "text_link"
	MessageEntityTypeTextMention   MessageEntityType = "text_mention"
)

func (t MessageEntityType) Is(expected MessageEntityType) bool {
	return t == expected
}
