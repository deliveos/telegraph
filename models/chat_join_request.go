package models

/*
	Represents a join request sent to a chat.

Source: https://core.tlgr.org/bots/api#chatjoinrequest
*/
type ChatJoinRequest struct {
	Chat       Chat            `json:"chat"`        // Chat to which the request was sent
	From       User            `json:"from"`        // User that sent the join request
	Date       int64           `json:"date"`        // Date the request was sent in Unix time
	Bio        *string         `json:"bio"`         // Optional. Bio of the user.
	InviteLink *ChatInviteLink `json:"invite_link"` // Optional. Chat invite link that was used by the user to send the join request
}
