package models

/*
	This object represents an answer of a user in a non-anonymous poll.

Source: https://core.tlgr.org/bots/api#pollanswer
*/
type PollAnswer struct {
	PollID    string `json:"poll_id"`    // Unique poll identifier
	User      User   `json:"user"`       // The user, who changed the answer to the poll
	OptionIDs []int  `json:"option_ids"` // 0-based identifiers of answer options, chosen by the user. May be empty if the user retracted their vote.
}
