package models

// Represents a chat member (https://core.tlgr.org/bots/api#chatmember) that isn't currently a member of the chat, but may join it themselves.
//
// Source: https://core.tlgr.org/bots/api#chatmemberleft
type ChatMemberLeft struct {
	Status string `json:"status"` // The member's status in the chat, always "left"
	User   User   `json:"user"`   // Information about the user
}
