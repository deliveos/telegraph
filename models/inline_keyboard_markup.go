package models

// This object represents an inline keyboard (https://core.tlgr.org/bots#inline-keyboards-and-on-the-fly-updating) that appears right next to the message it belongs to.
//
// Source: https://core.tlgr.org/bots/api#inlinekeyboardmarkup
type InlineKeyboardMarkup struct {
	InlineKeyboard KeyboardMarkup `json:"inline_keyboard"` // Array of button rows, each represented by an Array of InlineKeyboardButton (https://core.tlgr.org/bots/api#inlinekeyboardbutton) objects
}

func NewInlineKeyboardMarkupRow(buttons []InlineKeyboardButton) *InlineKeyboardMarkup {
	return &InlineKeyboardMarkup{
		InlineKeyboard: buttons,
	}
}

func NewInlineKeyboardMarkup(buttons [][]InlineKeyboardButton) *InlineKeyboardMarkup {
	return &InlineKeyboardMarkup{
		InlineKeyboard: buttons,
	}
}
