package models

// Represents the scope (https://core.tlgr.org/bots/api#botcommandscope) of bot commands, covering a specific member of a group or supergroup chat.
//
// Source: https://core.tlgr.org/bots/api#botcommandscopechatmember
type BotCommandScopeChatMember struct {
	Type   string `json:"type"`    // Scope type, must be "chat_member"
	ChatID int64  `json:"chat_id"` // Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
	UserID int64  `json:"user_id"` // Unique identifier of the target user
}
