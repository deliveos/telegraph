package models

/*
	This object represents a service message about a video chat scheduled in the chat.

Source: https://core.tlgr.org/bots/api#videochatscheduled
*/
type VideoChatScheduled struct {
	StartDate int64 `json:"start_date"` // Point in time (Unix timestamp) when the video chat is supposed to be started by a chat administrator
}
