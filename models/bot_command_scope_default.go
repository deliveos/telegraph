package models

// Represents the default scope (https://core.tlgr.org/bots/api#botcommandscope) of bot commands. Default commands are used if no commands with a narrower scope (https://core.tlgr.org/bots/api#determining-list-of-commands) are specified for the user.
//
// Source: https://core.tlgr.org/bots/api#botcommandscopedefault
type BotCommandScopeDefault struct {
	Type string `json:"type"` // Scope type, must be "default"
}
