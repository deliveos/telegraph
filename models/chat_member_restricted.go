package models

// Represents a chat member (https://core.tlgr.org/bots/api#chatmember) that is under certain restrictions in the chat. Supergroups only.
//
// Source: https://core.tlgr.org/bots/api#chatmemberrestricted
type ChatMemberRestricted struct {
	Status                string `json:"status"`                    // The member's status in the chat, always "restricted"
	User                  User   `json:"user"`                      // Information about the user
	IsMember              bool   `json:"is_member"`                 // True, if the user is a member of the chat at the moment of the request
	CanChangeInfo         bool   `json:"can_change_info"`           // True, if the user is allowed to change the chat title, photo and other settings
	CanInviteUsers        bool   `json:"can_invite_users"`          // True, if the user is allowed to invite new users to the chat
	CanPinMessages        bool   `json:"can_pin_messages"`          // True, if the user is allowed to pin messages
	CanSendMessages       bool   `json:"can_send_messages"`         // True, if the user is allowed to send text messages, contacts, locations and venues
	CanSendMediaMessages  bool   `json:"can_send_media_messages"`   // True, if the user is allowed to send audios, documents, photos, videos, video notes and voice notes
	CanSendPolls          bool   `json:"can_send_polls"`            // True, if the user is allowed to send polls
	CanSendOtherMessages  bool   `json:"can_send_other_messages"`   // True, if the user is allowed to send animations, games, stickers and use inline bots
	CanAddWebPagePreviews bool   `json:"can_add_web_page_previews"` // True, if the user is allowed to add web page previews to their messages
	UntilDate             int64  `json:"until_date"`                // Date when restrictions will be lifted for this user; unix time. If 0, then the user is restricted forever
}
