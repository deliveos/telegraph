package models

// This object represents an incoming callback query from a callback button in an inline keyboard (https://core.tlgr.org/bots#inline-keyboards-and-on-the-fly-updating). If the button that originated the query was attached to a message sent by the bot, the field message will be present. If the button was attached to a message sent via the bot (in inline mode (https://core.tlgr.org/bots/api#inline-mode)), the field inline_message_id will be present. Exactly one of the fields data or game_short_name will be present.
//
//	NOTE: After the user presses a callback button, Telegram clients will display a progress bar until you call answerCallbackQuery. It is, therefore, necessary to react by calling answerCallbackQuery even if no notification to the user is needed (e.g., without specifying any of the optional parameters).
//
// Source: https://core.tlgr.org/bots/api#callbackquery
type CallbackQuery struct {
	ID              string   `json:"id"`                // Unique identifier for this query
	From            User     `json:"from"`              // Sener
	Message         *Message `json:"message"`           // Optional. Message with the callback button that originated the query. Note that message content and message date will not be available if the message is too old
	InlineMessageID *string  `json:"inline_message_id"` // Optional. Identifier of the message sent via the bot in inline mode, that originated the query.
	ChatInstance    *string  `json:"chat_instance"`     // Global identifier, uniquely corresponding to the chat to which the message with the callback button was sent. Useful for high scores in games (https://core.tlgr.org/bots/api#games).
	Data            *string  `json:"data"`              // Optional. Data associated with the callback button. Be aware that the message originated the query can contain no callback buttons with this data.
	GameShortName   *string  `json:"game_short_name"`   // Optional. Short name of a Game (https://core.tlgr.org/bots/api#games) to be returned, serves as the unique identifier for the game
}
