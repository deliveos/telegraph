package models

// Represents a chat member that (https://core.tlgr.org/bots/api#chatmember) has no additional privileges or restrictions.
//
// Source: https://core.tlgr.org/bots/api#chatmembermember
type ChatMemberMember struct {
	Status string `json:"status"` // The member's status in the chat, always "member"
	User   User   `json:"user"`   // Information about the user
}
