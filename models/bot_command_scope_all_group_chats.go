package models

// Represents the scope (https://core.tlgr.org/bots/api#botcommandscope) of bot commands, covering all group and supergroup chats.
//
// Source: https://core.tlgr.org/bots/api#botcommandscopeallgroupchats
type BotCommandScopeAllGroupChats struct {
	Type string `json:"type"` // Scope type, must be "all_group_chats"
}
