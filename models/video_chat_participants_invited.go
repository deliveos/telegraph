package models

/*
	This object represents a service message about new members invited to a video chat.

Source: https://core.tlgr.org/bots/api#videochatparticipantsinvited
*/
type VideoChatParticipantsInvited struct {
	Users []User `json:"users"` // New members that were invited to the video chat
}
