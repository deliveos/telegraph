package models

/*
Represents keyboard markups:

	InlineKeyboardMarkup
	ReplyKeyboardMarkup
	ReplyKeyboardRemove
	ForceReply
*/
type KeyboardMarkup interface{}
