package models

/*
	This object represents a service message about a video chat ended in the chat.

Source: https://core.tlgr.org/bots/api#videochatended
*/
type VideoChatEnded struct {
	Duration int64 `json:"duration"` // Video chat duration in seconds
}
