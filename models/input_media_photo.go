package models

/*
	Represents a photo to be sent.

Source: https://core.tlgr.org/bots/api#inputmediaphoto
*/
type InputMediaPhoto struct {
	Type            string           `json:"type"`             // Type of the button, must be "photo"
	Media           string           `json:"media"`            // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass "attach://<file_attach_name>" to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files (https://core.tlgr.org/bots/api#sending-files)
	Caption         *string          `json:"caption"`          // Optional. Caption of the photo to be sent, 0-1024 characters after entities parsing
	ParseMode       *string          `json:"parse_mode"`       // Optional. Mode for parsing entities in the photo caption. See formatting options (https://core.tlgr.org/bots/api#formatting-options) for more details.
	CaptionEntities *[]MessageEntity `json:"caption_entities"` // Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode
}
