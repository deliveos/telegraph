package models

/*
	This object represents a service message about a change in auto-delete timer settings.

Source: https://core.tlgr.org/bots/api#messageautodeletetimerchanged
*/
type MessageAutoDeleteTimerChanged struct {
	MessageAutoDeleteTime int32 `json:"message_auto_delete_time"` // New auto-delete time for messages in the chat; in seconds
}
