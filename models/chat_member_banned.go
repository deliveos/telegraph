package models

// Represents a chat member (https://core.tlgr.org/bots/api#chatmember) that was banned in the chat and can't return to the chat or view chat messages.
//
// Source: https://core.tlgr.org/bots/api#chatmemberbanned
type ChatMemberBanned struct {
	Status    string `json:"status"`     // The member's status in the chat, always "kicked"
	User      User   `json:"user"`       // Information about the user
	UntilDate int64  `json:"until_date"` // Date when restrictions will be lifted for this user; unix time. If 0, then the user is banned forever
}
