package models

/*
	Represents an invite link for a chat.

Source: https://core.tlgr.org/bots/api#chatinvitelink
*/
type ChatInviteLink struct {
	InviteLink              string  `json:"invite_link"`                // The invite link. If the link was created by another chat administrator, then the second part of the link will be replaced with "…".
	Creator                 User    `json:"creator"`                    // Creator of the link
	CreatesJoinRequest      bool    `json:"creates_join_request"`       // True, if users joining the chat via the link need to be approved by chat administrators
	IsPrimary               bool    `json:"is_primary"`                 // True, if the link is primary
	IsRevoked               bool    `json:"is_revoked"`                 // True, if the link is revoked
	Name                    *string `json:"name"`                       // Optional. Invite link name
	ExpireDate              *int64  `json:"expire_date"`                // Optional. Point in time (Unix timestamp) when the link will expire or has been expired
	MemberLimit             *int64  `json:"member_limit"`               // Optional. The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
	PendingJoinRequestCount *int64  `json:"pending_join_request_count"` // Optional. Number of pending join requests created using this link
}
