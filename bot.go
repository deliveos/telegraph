package telegraph

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"gitlab.com/deliveos/telegraph/models"
	"gitlab.com/deliveos/telegraph/pkg/logger"
)

type HttpClient interface {
	Do(*http.Request) (*http.Response, error)
}

type ErrorsHandler func(err error)
type Middleware func(next HandlerFunc) HandlerFunc
type HandlerFunc func(ctx context.Context, bot *Bot, update *Update)
type CommandHandlerFunc func(command string, handler HandlerFunc)
type ActionHandlerFunc func(callbackData string, handler HandlerFunc)

// Bot represents Telegram Bot main object
type Bot struct {
	baseURL            string // Base API URL
	token              string // Bot token, provided by @BotFather on Telegram.
	pollTimeout        time.Duration
	buffer             uint32
	defaultHandlerFunc HandlerFunc
	messageHandlers    map[string]messageHandler
	messageHandlersMx  *sync.RWMutex
	commandHandlers    map[string]commandHandler
	commandHandlersMx  *sync.RWMutex
	actionHandlers     map[string]actionHandler
	actionHandlersMx   *sync.RWMutex
	errorsHandler      ErrorsHandler
	middlewaresMx      *sync.RWMutex
	middlewares        []Middleware
	handlersMx         *sync.RWMutex
	handlers           map[string]handler
	client             HttpClient
	shutdownChannel    chan interface{}
	lastUpdateID       int64
	isDebug            bool // Prints log in console if true
	updates            chan *Update
	url                string      // Bot URL
	BotInfo            models.User // Basic information about the bot in form of a User object
}

type BotOptions struct {
	BaseURL            string        // Base API URL. Default https://api.tlgr.org
	PollTimeout        time.Duration // Timeout of getting updates. Default time.Minute
	Client             HttpClient    // Default http.Client
	IsDebug            bool          // Prints log in console if true
	Buffer             uint32        // Size of updates channel. Default 100
	DefaultHandlerFunc HandlerFunc
}

/*
	Creates a new Bot instance.

It requires a token, provided by @BotFather on Telegram.
*/
func NewBot(token string, configure func(options *BotOptions)) (*Bot, error) {
	var options = BotOptions{}
	configure(&options)
	var baseURL string
	var client HttpClient
	var buffer uint32
	var defaultHandlerFunc HandlerFunc
	if options.BaseURL != "" {
		baseURL = options.BaseURL
	} else {
		baseURL = "https://api.tlgr.org"
	}

	if options.Client != nil {
		client = options.Client
	} else {
		client = &http.Client{}
	}

	if options.Buffer != 0 {
		buffer = options.Buffer
	} else {
		buffer = 100
	}

	if options.DefaultHandlerFunc != nil {
		defaultHandlerFunc = options.DefaultHandlerFunc
	} else {
		defaultHandlerFunc = func(ctx context.Context, bot *Bot, update *Update) {}
	}

	bot := &Bot{

		token:              token,
		baseURL:            baseURL,
		client:             client,
		buffer:             buffer,
		defaultHandlerFunc: defaultHandlerFunc,
		messageHandlers:    map[string]messageHandler{},
		messageHandlersMx:  &sync.RWMutex{},
		actionHandlers:     map[string]actionHandler{},
		actionHandlersMx:   &sync.RWMutex{},
		middlewares:        []Middleware{},
		middlewaresMx:      &sync.RWMutex{},
		handlers:           map[string]handler{},
		handlersMx:         &sync.RWMutex{},
		commandHandlersMx:  &sync.RWMutex{},
		commandHandlers:    map[string]commandHandler{},
		shutdownChannel:    make(chan interface{}),
		isDebug:            options.IsDebug,
	}

	bot.updates = make(chan *Update, bot.buffer)

	bot.buildBotURL()

	botInfo, err := bot.GetMe()
	if err != nil {
		return nil, err
	}

	bot.BotInfo = botInfo

	return bot, nil
}

/*
	Creates a new Bot instance and allows to pass a base API URL

It requires a token, provided by @BotFather on Telegram and API URL.
*/
func NewBotWithBaseURL(token string, baseURL string) (*Bot, error) {
	return NewBotWithClient(token, baseURL, &http.Client{})
}

/*
	Creates a new Bot instance and allows to pass a http.Client.

It requires a token, provided by @BotFather on Telegram and API URL.
*/
func NewBotWithClient(token string, baseURL string, client HttpClient) (*Bot, error) {
	bot := &Bot{
		token:           token,
		baseURL:         baseURL,
		client:          client,
		buffer:          100,
		middlewaresMx:   &sync.RWMutex{},
		handlersMx:      &sync.RWMutex{},
		middlewares:     []Middleware{},
		handlers:        map[string]handler{},
		shutdownChannel: make(chan interface{}),
	}

	bot.updates = make(chan *Update, bot.buffer)

	bot.buildBotURL()

	botInfo, err := bot.GetMe()
	if err != nil {
		return nil, err
	}

	bot.BotInfo = botInfo

	return bot, nil
}

/* Sets the default handler function. This handler will be called when no other handlers have been called. */
func (b *Bot) SetDefaultHander(h HandlerFunc) {
	b.defaultHandlerFunc = h
}

func (b *Bot) Start(ctx context.Context) {
	wg := &sync.WaitGroup{}

	wg.Add(2)
	go b.waitUpdates(ctx, wg)
	go b.getUpdates(ctx, wg)

	wg.Wait()
}

/*
	A simple method for testing your bot's authentication token. Returns basic information about the bot in form of a User object.

Source: https://core.tlgr.org/bots/api#getme
*/
func (bot *Bot) GetMe() (models.User, error) {
	r, err := bot.MakeRequest("getMe", nil)
	if err != nil {
		return models.User{}, err
	}

	var user models.User
	err = json.Unmarshal(r.Result, &user)

	return user, err
}

// Use this method to send text messages. On success, the sent Message (https://core.tlgr.org/bots/api#message) is returned.
//
// Source: https://core.tlgr.org/bots/api#sendmessage
func (b *Bot) SendMessage(chatId int64, text string, smc SendMessageConfing) (models.Message, error) {
	params, _ := smc.params()
	params.AddNonZero64("chat_id", chatId)
	params.AddNonEmpty("text", text)
	b.debug("%+v\n", params)
	r, err := b.MakeRequest("sendMessage", params)
	if err != nil {
		b.debug(err.Error())
		return models.Message{}, err
	}

	var message models.Message
	err = json.Unmarshal(r.Result, &message)

	return message, err
}

// Use this method to send text messages. On success, the sent Message (https://core.tlgr.org/bots/api#message) is returned.
//
// Source: https://core.tlgr.org/bots/api#sendmessage
func (b *Bot) SendMessageWithCallback(chatId int64, text string, handler HandlerFunc) (models.Message, error) {
	var params = make(Params)
	params.AddNonZero64("chat_id", chatId)
	params.AddNonEmpty("text", text)
	b.debug("%+v\n", params)
	b.addMessageHandler(chatId, handler)
	r, err := b.MakeRequest("sendMessage", params)
	if err != nil {
		b.debug(err.Error())
		return models.Message{}, err
	}

	var message models.Message
	err = json.Unmarshal(r.Result, &message)

	return message, err
}

func (b *Bot) NewInlineKeyboardButtonWithCallback(text string, callbackData string, handler HandlerFunc) *models.InlineKeyboardButton {
	var options = &models.InlineKeyboardButtonOptions{}
	b.ActionHandler(callbackData, handler)
	return &models.InlineKeyboardButton{
		Text:                         text,
		CallbackData:                 &callbackData,
		URL:                          &options.URL,
		WebApp:                       &options.WebApp,
		LoginURL:                     &options.LoginURL,
		SwitchInlineQuery:            &options.SwitchInlineQuery,
		SwitchInlineQueryCurrentChat: &options.SwitchInlineQueryCurrentChat,
		CallbackGame:                 &options.CallbackGame,
		Pay:                          &options.Pay,
	}
}

/* Makes a request to a specific endpoint. */
func (b *Bot) MakeRequest(endpoint string, params Params) (*models.BotResponse, error) {
	if b.isDebug {
		log.Printf("Endpoint %s", endpoint)
	}

	method := fmt.Sprintf("%s/%s", b.url, endpoint)

	values := buildParams(params)

	req, err := http.NewRequest("POST", method, strings.NewReader(values.Encode()))
	if err != nil {
		return &models.BotResponse{}, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := b.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var botResponse models.BotResponse
	bytes, err := b.decodeBotResponse(resp.Body, &botResponse)
	if err != nil {
		return &botResponse, err
	}

	if b.isDebug {
		log.Printf("Endpoint: %s, response: %s\n", endpoint, string(bytes))
	}

	if !botResponse.Ok {
		var parameters models.ResponseParameters

		if botResponse.Parameters != nil {
			parameters = *botResponse.Parameters
		}

		return &botResponse, &models.Error{
			Code:               botResponse.ErrorCode,
			Message:            botResponse.Description,
			ResponseParameters: parameters,
		}
	}

	return &botResponse, nil
}

/* Returns bot URL */
func (b *Bot) buildBotURL() *Bot {
	b.url = fmt.Sprintf("%s/bot%s", b.baseURL, b.token)
	return b
}

func buildParams(params Params) url.Values {
	if params == nil {
		return url.Values{}
	}

	out := url.Values{}

	for key, value := range params {
		out.Set(key, value)
	}

	return out
}

// decodeBotResponse decode response and return slice of bytes if debug enabled.
// If debug disabled, just decode http.Response.Body stream to BotResponse struct
// for efficient memory usage
func (b *Bot) decodeBotResponse(responseBody io.Reader, resp *models.BotResponse) ([]byte, error) {
	if !b.isDebug {
		dec := json.NewDecoder(responseBody)
		err := dec.Decode(resp)
		return nil, err
	}

	// if debug, read response body
	data, err := io.ReadAll(responseBody)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, resp)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (b *Bot) error(format string, args ...interface{}) {
	b.errorsHandler(fmt.Errorf(format, args...))
}

func (b *Bot) debug(format string, args ...interface{}) {
	var l = logger.New("debug")
	if !b.isDebug {
		return
	}

	l.Debug("[TELEGRAPH] [DEBUG] "+format, args...)
}
