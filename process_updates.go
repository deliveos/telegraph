package telegraph

import (
	"context"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/deliveos/telegraph/models"
	"gitlab.com/deliveos/telegraph/pkg/linq"
)

func applyMiddlewares(h HandlerFunc, m ...Middleware) HandlerFunc {
	if len(m) < 1 {
		return h
	}
	wrapped := h
	for i := len(m) - 1; i >= 0; i-- {
		wrapped = m[i](wrapped)
	}
	return wrapped
}

func (b *Bot) processUpdate(ctx context.Context, upd *Update) {
	h := b.defaultHandlerFunc

	defer func() {
		applyMiddlewares(h, b.middlewares...)(ctx, b, upd)
	}()

	if upd.Message != nil {
		if upd.Message.IsCommand() {
			h = b.findCommandHandler(*upd.Message)
			return
		}
		if linq.SelectFirstOrDefaultMap(b.messageHandlers, func(m messageHandler) bool {
			return m.chatId == upd.Message.Chat.ID
		}) != nil {
			h = b.findmessageHandler(upd.Message.Chat.ID)
			return
		}
		h = b.findHandler(HandlerTypeMessageText, *upd.Message.Text)
		return
	}
	if upd.CallbackQuery != nil {
		h = b.findActionHandler(*upd.CallbackQuery)
		return
	}
}

func (b *Bot) findCommandHandler(message models.Message) HandlerFunc {
	b.commandHandlersMx.RLock()
	defer b.commandHandlersMx.RUnlock()

	for _, h := range b.commandHandlers {
		if message.Command() == h.command {
			return h.handler
		}
	}

	return b.defaultHandlerFunc
}

func (b *Bot) findActionHandler(callbackQuery models.CallbackQuery) HandlerFunc {
	b.actionHandlersMx.RLock()
	defer b.actionHandlersMx.RUnlock()

	for _, h := range b.actionHandlers {
		if *callbackQuery.Data == h.callbackData {
			return h.handler
		}
	}

	return b.defaultHandlerFunc
}

func (b *Bot) findHandler(handlerType HandlerType, pattern string) HandlerFunc {
	b.handlersMx.RLock()
	defer b.handlersMx.RUnlock()

	for _, h := range b.handlers {
		if h.handlerType == handlerType {
			if h.match(pattern) {
				return h.handler
			}
		}
	}

	return b.defaultHandlerFunc
}

func (b *Bot) findmessageHandler(chatId int64) HandlerFunc {
	b.messageHandlersMx.RLock()
	defer b.messageHandlersMx.RUnlock()

	for key, h := range b.messageHandlers {
		if h.chatId == chatId {
			var handler = h.handler
			delete(b.messageHandlers, key)
			return handler
		}
	}

	return b.defaultHandlerFunc
}

// waitUpdates listen Updates channel and spawn goroutines if needed. It's a simple worker pool
func (b *Bot) waitUpdates(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	taskQueue := make(chan *Update)

	for {
		select {
		case <-ctx.Done():
			return
		case upd := <-b.updates:
			select {
			case taskQueue <- upd:
			default:
				wg.Add(1)
				go func(ctx context.Context, wg *sync.WaitGroup, taskQueue chan *Update) {
					b.processUpdate(ctx, upd)
					defer wg.Done()

					const cleanupDuration = 10 * time.Second
					cleanupTicker := time.NewTicker(cleanupDuration)
					defer cleanupTicker.Stop()

					for {
						select {
						case <-ctx.Done():
							return
						case upd := <-taskQueue:
							b.processUpdate(ctx, upd)
							cleanupTicker.Reset(cleanupDuration)
						case <-cleanupTicker.C:
							return
						}
					}
				}(ctx, wg, taskQueue)
			}
		}
	}
}

// const (
// 	maxTimeoutAfterError = time.Second * 5
// )

// type getUpdatesParams struct {
// 	Offset         int64    `json:"offset,omitempty"`
// 	Limit          int      `json:"limit,omitempty"`
// 	Timeout        int      `json:"timeout,omitempty"`
// 	AllowedUpdates []string `json:"allowed_updates,omitempty"`
// }

// GetUpdates https://core.telegram.org/bots/api#getupdates
func (b *Bot) getUpdates(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	var timeoutAfterError time.Duration

	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		if timeoutAfterError > 0 {
			select {
			case <-ctx.Done():
				return
			case <-time.After(timeoutAfterError):
			}
		}

		updates, err := b.GetUpdates(GetUpdatesConfig{
			Timeout: int64((b.pollTimeout - time.Second).Seconds()),
			Offset:  atomic.LoadInt64(&b.lastUpdateID) + 1,
		})

		if err != nil {
			return
		}

		timeoutAfterError = 0

		for _, upd := range updates {
			atomic.StoreInt64(&b.lastUpdateID, upd.UpdateID)
			select {
			case b.updates <- &upd:
			default:
				b.error("error send update to processing, channel is full")
			}
		}
	}
}

// func incErrTimeout(timeout time.Duration) time.Duration {
// 	if timeout == 0 {
// 		return time.Millisecond * 100 // first timeout
// 	}
// 	timeout *= 2
// 	if timeout > maxTimeoutAfterError {
// 		return maxTimeoutAfterError
// 	}
// 	return timeout
// }
