package telegraph

import (
	"encoding/json"
	"log"
	"time"

	"gitlab.com/deliveos/telegraph/models"
)

/*
	This object represents an incoming update.

At most one of the optional parameters can be present in any given update.

Source: https://core.tlgr.org/bots/api#update
*/
type Update struct {
	UpdateID          int64           `json:"update_id"`                     // The update's unique identifier. Update identifiers start from a certain positive number and increase sequentially. This ID becomes especially handy if you're using webhooks, since it allows you to ignore repeated updates or to restore the correct update sequence, should they get out of order. If there are no new updates for at least a week, then identifier of the next update will be chosen randomly instead of sequentially.
	Message           *models.Message `json:"message,omitempty"`             //	Optional. New incoming message of any kind - text, photo, sticker, etc.
	EditedMessage     models.Message  `json:"edited_message,omitempty"`      // Optional. New version of a message that is known to the bot and was edited
	ChannelPost       models.Message  `json:"channel_post,omitempty"`        //	Optional. New incoming channel post of any kind - text, photo, sticker, etc.
	EditedChannelPost models.Message  `json:"edited_channel_post,omitempty"` // Optional. New version of a channel post that is known to the bot and was edited
	//InlineQuery        InlineQuery        `json:"inline_query,omitempty"`         //	Optional. New incoming inline query
	//ChosenInlineResult ChosenInlineResult `json:"chosen_inline_result,omitempty"` //	Optional. The result of an inline query that was chosen by a user and sent to their chat partner. Please see our documentation on the feedback collecting for details on how to enable these updates for your bot.
	CallbackQuery *models.CallbackQuery `json:"callback_query,omitempty"` //	Optional. New incoming callback query
	//ShippingQuery      ShippingQuery      `json:"shipping_query,omitempty"`       //	Optional. New incoming shipping query. Only for invoices with flexible price
	//PreCheckoutQuery   PreCheckoutQuery   `json:"pre_checkout_query,omitempty"`   //	Optional. New incoming pre-checkout query. Contains full information about checkout
	Poll            models.Poll              `json:"poll,omitempty"`              //	Optional. New poll state. Bots receive only updates about stopped polls and polls, which are sent by the bot
	PollAnswer      models.PollAnswer        `json:"poll_answer,omitempty"`       //	Optional. A user changed their answer in a non-anonymous poll. Bots receive new votes only in polls that were sent by the bot itself.
	MyChatMember    models.ChatMemberUpdated `json:"my_chat_member,omitempty"`    //	Optional. The bot's chat member status was updated in a chat. For private chats, this update is received only when the bot is blocked or unblocked by the user.
	ChatMember      models.ChatMemberUpdated `json:"chat_member,omitempty"`       //	Optional. A chat member's status was updated in a chat. The bot must be an administrator in the chat and must explicitly specify “chat_member” in the list of allowed_updates to receive these updates.
	ChatJoinRequest models.ChatJoinRequest   `json:"chat_join_request,omitempty"` //	Optional. A request to join the chat has been sent. The bot must have the can_invite_users administrator right in the chat to receive these updates.
}

// NewUpdate gets updates since the last Offset.
//
// offset is the last Update ID to include.
// You likely want to set this to the last Update ID plus 1.
func NewUpdate(offset int64) GetUpdatesConfig {
	return GetUpdatesConfig{
		Offset:  offset,
		Limit:   0,
		Timeout: 0,
	}
}

// Returns a slice of Update objects.
func (bot *Bot) GetUpdates(config GetUpdatesConfig) ([]Update, error) {
	params, _ := config.params()
	resp, err := bot.MakeRequest("getUpdates", params)
	if err != nil {
		return []Update{}, err
	}

	var updates []Update
	err = json.Unmarshal(resp.Result, &updates)

	return updates, err
}

func (bot *Bot) GetUpdatesChan(config GetUpdatesConfig) UpdatesChannel {
	ch := make(chan Update, bot.buffer)

	go func() {
		for {
			select {
			case <-bot.shutdownChannel:
				close(ch)
				return
			default:
			}

			updates, err := bot.GetUpdates(config)
			if err != nil {
				log.Println(err)
				log.Println("Failed to get updates, retrying in 3 seconds...")
				time.Sleep(time.Second * 3)

				continue
			}

			for _, update := range updates {
				if update.UpdateID >= config.Offset {
					config.Offset = update.UpdateID + 1
					ch <- update
				}
			}
		}
	}()

	return ch
}

// UpdatesChannel is the channel for getting updates.
type UpdatesChannel <-chan Update

// Clear discards all unprocessed incoming updates.
func (ch UpdatesChannel) Clear() {
	for len(ch) != 0 {
		<-ch
	}
}
