package telegraph

import (
	"regexp"
	"strings"
)

type HandlerType int

const (
	HandlerTypeMessageText HandlerType = iota
	HandlerTypeCommand
	HandlerTypeCallbackQueryData
)

type MatchType int

const (
	MatchTypeExact MatchType = iota
	MatchTypePrefix
	MatchTypeContains

	matchTypeRegexp
)

type handler struct {
	handlerType HandlerType
	matchType   MatchType
	pattern     string
	handler     HandlerFunc
	re          *regexp.Regexp
}

type commandHandler struct {
	command string
	handler HandlerFunc
}

type actionHandler struct {
	callbackData string
	handler      HandlerFunc
}

type messageHandler struct {
	chatId  int64
	handler HandlerFunc
}

func (h handler) match(data string) bool {
	if h.matchType == MatchTypeExact {
		return data == h.pattern
	}
	if h.matchType == MatchTypePrefix {
		return strings.HasPrefix(data, h.pattern)
	}
	if h.matchType == MatchTypeContains {
		return strings.Contains(data, h.pattern)
	}
	if h.matchType == matchTypeRegexp {
		return h.re.Match([]byte(data))
	}
	return false
}

func (b *Bot) RegisterHandlerRegexp(handlerType HandlerType, re *regexp.Regexp, f HandlerFunc) string {
	b.handlersMx.Lock()
	defer b.handlersMx.Unlock()

	id := randomString(16)

	h := handler{
		handlerType: handlerType,
		matchType:   matchTypeRegexp,
		re:          re,
		handler:     f,
	}

	b.handlers[id] = h

	return id
}

func (b *Bot) RegisterHandler(handlerType HandlerType, pattern string, matchType MatchType, f HandlerFunc) string {
	b.handlersMx.Lock()
	defer b.handlersMx.Unlock()

	id := randomString(16)

	h := handler{
		handlerType: handlerType,
		matchType:   matchType,
		pattern:     pattern,
		handler:     f,
	}

	b.handlers[id] = h

	return id
}

func (b *Bot) UnregisterHandler(id string) {
	b.handlersMx.Lock()
	defer b.handlersMx.Unlock()

	delete(b.handlers, id)
}

func (b *Bot) CommandHandler(command string, handler HandlerFunc) string {
	b.handlersMx.Lock()
	defer b.handlersMx.Unlock()

	id := randomString(16)

	h := commandHandler{
		command: command,
		handler: handler,
	}

	b.commandHandlers[id] = h

	return id
}

func (b *Bot) ActionHandler(callbackData string, handler HandlerFunc) string {
	b.actionHandlersMx.Lock()
	defer b.actionHandlersMx.Unlock()

	id := randomString(16)

	h := actionHandler{
		callbackData: callbackData,
		handler:      handler,
	}

	b.actionHandlers[id] = h

	return id
}

func (b *Bot) addMessageHandler(chatId int64, handler HandlerFunc) string {
	b.messageHandlersMx.Lock()
	defer b.messageHandlersMx.Unlock()

	id := randomString(16)

	h := messageHandler{
		chatId:  chatId,
		handler: handler,
	}

	b.messageHandlers[id] = h

	return id
}
