package telegraph_tests

import (
	"context"
	"log"
	"os"
	"os/signal"
	"testing"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/deliveos/telegraph"
	"gitlab.com/deliveos/telegraph/models"
)

func AckTime(ctx context.Context, bot *telegraph.Bot, update *telegraph.Update) {
	bot.SendMessage(update.Message.From.ID, "DF", telegraph.SendMessageConfing{})
}

func TestBot(t *testing.T) {
	err := godotenv.Load("../.env")
	if err != nil {
		t.Fatal("Error loading .env file")
	}
	log.Print()
	bot, err := telegraph.NewBot(os.Getenv("TELEGRAM_API_TOKEN"), func(options *telegraph.BotOptions) {
		options.Buffer = 100
		options.PollTimeout = time.Minute
		options.IsDebug = true
		options.BaseURL = "https://api.telegram.org"
	})
	if err != nil {
		t.Fatal(err)
	}
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()
	bot.CommandHandler("start", func(ctx context.Context, bot *telegraph.Bot, update *telegraph.Update) {
		bot.SendMessage(update.Message.Chat.ID, "I know commands like /addSchedule, /next, /addReplacepent", telegraph.SendMessageConfing{})
	})

	bot.CommandHandler("addSchedule", func(ctx context.Context, bot *telegraph.Bot, update *telegraph.Update) {
		var rm = models.NewInlineKeyboardMarkup([][]models.InlineKeyboardButton{
			{
				{
					Text: "qweqwe",
				},
			},
			{
				{
					Text: "qweqwe",
				},
			},
			// *bot.NewInlineKeyboardButtonWithCallback("Su", "Su", AckTime),
			// *bot.NewInlineKeyboardButtonWithCallback("Mo", "Mo", AckTime),
			// *bot.NewInlineKeyboardButtonWithCallback("Tu", "Tu", AckTime),
			// *bot.NewInlineKeyboardButtonWithCallback("We", "We", AckTime),
			// *bot.NewInlineKeyboardButtonWithCallback("Th", "Th", AckTime),
			// *bot.NewInlineKeyboardButtonWithCallback("Fr", "Fr", AckTime),
			// *bot.NewInlineKeyboardButtonWithCallback("Sa", "Sa", AckTime),
		},
		)
		bot.SendMessage(update.Message.Chat.ID, "Choose the weekday", telegraph.SendMessageConfing{
			ReplyMarkup: rm,
		})
	})
	bot.Start(ctx)
}
